<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetPageProperty("hide_title", "Y");
$APPLICATION->SetTitle("ЗАО «Промтрактор-вагон»");
?>
    <section id="company-section" class="grid">
        <div class="golden-large unit wrapper">
            <h1 class="title">ЗАО «Промтрактор-вагон»</h1>
            <h2>Изготовление и капитальный ремонт грузовых вагонов</h2>
            <p>
                Ведущее вагоностроительное предприятие машиностроительно-индустриальной группы «Концерн «Тракторные
                заводы». Завод является партнером ведущих транспортных операторов и одним из наиболее современных и
                технологичных производств в России.
            </p>
            <p>
                Предприятие специализируется на изготовлении и осуществлении капитального ремонта грузовых вагонов.
            </p>
            <p>
                <a href="/company" class="button about">Подробнее о компании</a>
                <a href="#video-about-us" class="transparent button colorbox-inline icon-video video" rel="colorbox">Видео про нас</a>
            </p>
        </div>
        <div class="hidden">
            <div id="video-about-us">
                <video  controls="controls" src="/upload/content/about.mp4"></video>
            </div>
        </div>
    </section>
    <section id="production-section" class="grid">

        <div class="grid">
            <div class="whole unit header">
                <h2>Производим продукцию только <span class="dark-red">высшего качества</span></h2>
                <p>
                    Ведущее вагоностроительное предприятие машиностроительно-индустриальной группы «Концерн «Тракторные
                    заводы». Завод является партнером ведущих транспортных операторов и одним из наиболее современных и
                    технологичных производств в России.Предприятие специализируется на изготовлении и осуществлении
                    капитального ремонта грузовых вагонов. <br>
                </p>
            </div>
        </div>

        <? $APPLICATION->IncludeComponent(
	"bitrix:catalog.top", 
	"front-slider", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"BASKET_URL" => "/personal/basket.php",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"DETAIL_URL" => "",
		"DISPLAY_COMPARE" => "N",
		"ELEMENT_COUNT" => "9",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "1c_catalog",
		"LINE_ELEMENT_COUNT" => "3",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"OFFERS_LIMIT" => "3",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"ROTATE_TIMER" => "30",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SEF_MODE" => "N",
		"SEF_RULE" => "",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "blue",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"VIEW_MODE" => "SLIDER",
		"COMPONENT_TEMPLATE" => "front-slider",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"SHOW_PAGINATION" => "N"
	),
	false
); ?>

    </section>
    <section id="tech-section" class="grid">
        <div class="three-quarters unit align-right description">
            <h2><span class="dark-red">Новые технологии</span> производства в вагоностроении</h2>
            <p>
                Ведущее вагоностроительное предприятие машиностроительно-индустриальной группы «Концерн «Тракторные
                заводы».
            </p>
            <p>
                Завод является партнером ведущих транспортных операторов и одним из наиболее современных и технологичных
                производств в России.
            </p>
            <p>
                Предприятие специализируется на изготовлении и осуществлении капитального ремонта грузовых вагонов.
            </p>
        </div>

        <div class="one-quarter unit">
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "section",
                Array()
            ); ?>
        </div>

        <div id="production" class="hidden section-info ">
            <a href="/" class="close-btn"><img src="<?= SITE_TEMPLATE_PATH?>/img/close.png" alt=""></a>
            <h2>Производство</h2>
            <p>Производство грузовых вагонов осуществляется в новом универсальном сборочно-сварочном корпусе (УССК), запуск которого состоялся в 2010 году. Серийное производство полувагонов модели 12-1302 было организовано в начале 2007 года на высвобожденных путем оптимизации производственных потоков площадях вагоноремонтного производства, оснащенных оборудованием для изготовления вагонов. Производственные мощности по выпуску полувагонов составляли более 100 единиц в месяц. </p>
            <a href="/production" class="more">Узнать больше</a>
        </div>

        <div id="repairs" class="hidden section-info">
            <a href="/repairs" class="close-btn"><img src="<?= SITE_TEMPLATE_PATH?>/img/close.png" alt=""></a>
            <h2>Ремонт</h2>
            <p>ЗАО «Промтрактор-Вагон» предлагает:

                Проведение полного комплекса ремонтных работ (капитальный, деповской) следующих видов подвижного состава</p>

            <a href="/repairs" class="more">Узнать больше</a>
        </div>
        
        <span class="note hide-on-mobiles">Вагон-хоппер используется для демонстрации</span>


        <img id="hopper-car" class="hide-on-mobiles" src="/upload/content/hopper-car.png">

    </section>

    <section id="news-section" class="grid">

<? $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"news", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "news",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "3",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC"
	),
	false
); ?> </section><? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>