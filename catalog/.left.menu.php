<?
global $APPLICATION;
$aMenuLinksExt = array();

if(CModule::IncludeModule('iblock')) {
    $arFilter = array(
        "IBLOCK_CODE" => "catalog"
    );

    $dbIBlock = CIBlockSection::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter);

    while($arIBlock = $dbIBlock->GetNext())
    {
        $aMenuLinksExt[] = array(
            $arIBlock['NAME'],
            $arIBlock['SECTION_PAGE_URL'] . "/",
            array(),
            array(),
            ""
        );
    }

}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>