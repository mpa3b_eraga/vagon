<h2 class="title">Контакты</h2>
<p>
    Наше предприятие приглашает Вас к сотрудничеству в сфере строительства, производства и ремонта грузовых вагонов,
    вагонных тележек, запасных частей для подвижного состава железнодорожного транспорта.
</p>
<div id="contacts" class="grid">
    <div class="half unit">

        <div class="icon icon-location">
            <p>429332, Чувашская Республика, <a href="#contacts-map" title="ЗАО «Промтрактор-Вагон» на карте" class="colorbox colorbox-inline">г.
                    Канаш, ул.
                    Ильича, 1а</a>, ЗАО «Промтрактор-Вагон».</p>
        </div>

        <div class="icon icon-phone">
            <p>
                <a href="tel:88353325565">+7 (83533) 2-55-65</a><br>
                <a href="tel:88353328600">+7 (83533) 2-86-00</a> (приёмная)
            </p>
        </div>

        <div class="icon icon-mail">
            <p>
                <a href="mailto:kvrz@cbx.ru">kvrz@cbx.ru</a>
            </p>
        </div>

        <? $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "contacts",
            Array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "COMPONENT_TEMPLATE" => "contacts",
                "DELAY" => "N",
                "MAX_LEVEL" => "1",
                "MENU_CACHE_GET_VARS" => array(),
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "contact",
                "USE_EXT" => "N"
            )
        ); ?>
    </div>
    <div class="half unit">

        <h3 class="title">Свяжитесь с нами</h3>

        <? $APPLICATION->IncludeComponent(
            "bitrix:main.feedback",
            "contact",
            Array(
                "COMPONENT_TEMPLATE" => "contact",
                "EMAIL_TO" => "admin@promtractor-vagon.ru",
                "EVENT_MESSAGE_ID" => array(0 => "7",),
                "OK_TEXT" => "Спасибо, ваше сообщение принято.",
                "REQUIRED_FIELDS" => array(0 => "MESSAGE",),
                "USE_CAPTCHA" => "Y"
            )
        ); ?>

    </div>
</div>

<div id="partners" class="grid align-center hide-on-mobiles">
    <div class="one-quarter unit">
        <img src="/upload/content/partner-brunswick.png">
    </div>
    <div class="one-quarter unit">
        <img src="/upload/content/partner-rth.png">
    </div>
    <div class="one-quarter unit">
        <img src="/upload/content/partner-rzhd.png">
    </div>
    <div class="one-quarter unit">
        <img src="/upload/content/partner-veb-leasing.png">
    </div>
</div>

<div class="hidden">
    <div id="contacts-map">
        <? $APPLICATION->IncludeComponent(
            "bitrix:map.yandex.view",
            ".default",
            array(
                "COMPONENT_TEMPLATE" => ".default",
                "CONTROLS" => array(
                    0 => "TYPECONTROL",
                    1 => "SCALELINE",
                ),
                "INIT_MAP_TYPE" => "MAP",
                "MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.51976608868919;s:10:\"yandex_lon\";d:47.527448597900396;s:12:\"yandex_scale\";i:15;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:47.527212563507;s:3:\"LAT\";d:55.519437392104;s:4:\"TEXT\";s:44:\"ЗАО «Промтрактор-Вагон»\";}}}",
                "MAP_HEIGHT" => "320",
                "MAP_ID" => "",
                "MAP_WIDTH" => "480",
                "OPTIONS" => array(
                    0 => "ENABLE_SCROLL_ZOOM",
                    1 => "ENABLE_DRAGGING",
                )
            ),
            false
        ); ?>
    </div>
</div>
<br>