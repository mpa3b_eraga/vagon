<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<div id="feedback-form" class="center-on-mobiles">

    <? if (!empty($arResult["ERROR_MESSAGE"])) { ?>
        <p class="message error"><? foreach ($arResult["ERROR_MESSAGE"] as $v) ShowError($v); ?></p>
    <? } ?>

    <? if (strlen($arResult["OK_MESSAGE"]) > 0) { ?>
        <p class="message ok"><? echo $arResult["OK_MESSAGE"] ?></p>
    <? } ?>

    <form action="<? echo POST_FORM_ACTION_URI ?>" method="POST">

        <? echo bitrix_sessid_post() ?>

        <div id="feedback-name" class="feedback-form-field">

            <? /*
            <div class="caption">
                <? echo GetMessage("NAME") ?>

                <? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])): ?>
                    <span class="feedback-req">*</span>
                <? endif ?>
            </div>
            */ ?>

            <input type="text" name="user_name" <? /* value="<? echo $arResult["AUTHOR_NAME"] ?>" */ ?> value="" placeholder="<? echo GetMessage("NAME") ?>">

        </div>

        <div id="feedback-email" class="feedback-form-field">

            <? /*
            <div class="caption">
                <? echo GetMessage("EMAIL") ?>

                <? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])): ?>
                    <span class="feedback-req">*</span>
                <? endif ?>
            </div>
            */ ?>

            <input type="text" name="user_email" <? /* value="<? echo $arResult["AUTHOR_EMAIL"] ?>" */ ?> value="" placeholder="<? echo GetMessage("EMAIL") ?>">

        </div>

        <div id="feedback-message" class="feedback-form-field">

            <? /*
            <div class="caption">
                <? echo GetMessage("MESSAGE") ?>

                <? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])): ?>
                    <span class="feedback-req">*</span>
                <? endif ?>
            </div>
            */ ?>

            <textarea name="MESSAGE" rows="5" cols="40" placeholder="<? echo GetMessage("MESSAGE") ?>"></textarea>

        </div>

        <? if ($arParams["USE_CAPTCHA"] == "Y"): ?>

            <div id="feedback-captcha" class="feedback-form-field">

                <div class="caption"><? echo GetMessage("CAPTCHA") ?></div>

                <input type="hidden" name="captcha_sid" value="<? echo $arResult["capCode"] ?>">

                <img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["capCode"] ?>" width="180" height="40" alt="CAPTCHA">

                <div class="caption"><? echo GetMessage("CAPTCHA_CODE") ?><span class="feedback-req">*</span></div>

                <input type="text" name="captcha_word" size="30" maxlength="50" value="">

            </div>

        <? endif; ?>

        <input type="hidden" name="PARAMS_HASH" value="<? echo $arResult["PARAMS_HASH"] ?>">

        <input type="submit" name="submit" value="<? echo GetMessage("SUBMIT") ?>">

    </form>

</div>

<? /*

<div id="feedback-form">

    <? if (!empty($arResult["ERROR_MESSAGE"])) { ?>
        <p class="message error">
            <? foreach ($arResult["ERROR_MESSAGE"] as $v) ShowError($v); ?>
        </p>
    <? } ?>

    <? if (strlen($arResult["OK_MESSAGE"]) > 0) { ?>
        <p class="message ok"><? echo $arResult["OK_MESSAGE"] ?></p>
    <? } ?>

    <form action="<? echo POST_FORM_ACTION_URI ?>" method="POST">

        <? echo bitrix_sessid_post() ?>

        <div id="feedback-name" class="feedback-form-field">

            <div class="caption">
                <? echo GetMessage("NAME") ?>

                <? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])): ?>
                    <span class="feedback-req">*</span>
                <? endif ?>
            </div>

            <input type="text" name="user_name" value="<? echo $arResult["AUTHOR_NAME"] ?>" placeholder="<? echo GetMessage("NAME") ?>">

        </div>

        <div id="feedback-email" class="feedback-form-field">

            <div class="caption">
                <? echo GetMessage("EMAIL") ?>

                <? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])): ?>
                    <span class="feedback-req">*</span>
                <? endif ?>
            </div>

            <input type="text" name="user_email" value="<? echo $arResult["AUTHOR_EMAIL"] ?>" placeholder="<? echo GetMessage("EMAIL") ?>">

        </div>

        <div id="feedback-message" class="feedback-form-field">

            <div class="caption">
                <? echo GetMessage("MESSAGE") ?>

                <? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])): ?>
                    <span class="feedback-req">*</span>
                <? endif ?>
            </div>

            <textarea name="MESSAGE" rows="5" cols="40" placeholder="<? echo GetMessage("MESSAGE") ?>">
                <? echo $arResult["MESSAGE"] ?>
            </textarea>

        </div>

        <? if ($arParams["USE_CAPTCHA"] == "Y"): ?>

            <div id="feedback-captcha" class="feedback-form-field">

                <div class="caption"><? echo GetMessage("CAPTCHA") ?></div>

                <input type="hidden" name="captcha_sid" value="<? echo $arResult["capCode"] ?>">

                <img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["capCode"] ?>" width="180" height="40" alt="CAPTCHA">

                <div class="caption"><? echo GetMessage("CAPTCHA_CODE") ?><span class="feedback-req">*</span></div>

                <input type="text" name="captcha_word" size="30" maxlength="50" value="">

            </div>

        <? endif; ?>

        <input type="hidden" name="PARAMS_HASH" value="<? echo $arResult["PARAMS_HASH"] ?>">

        <input type="submit" name="submit" value="<? echo GetMessage("SUBMIT") ?>">

    </form>

</div>

 */ ?>