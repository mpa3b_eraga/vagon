<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<article id="<? echo $arResult['IBLOCK_CODE'] . '-' . $arResult['ID']; ?>" class="car grid">

    <div class="header">

        <? // название элемента раздела из пользовательского свойства
        $rsElementName = CIBlockSection::GetList(
            array(""),
            array(
                "IBLOCK_ID" => $arResult['IBLOCK_ID'],
                "CODE" => $arResult['SECTION']['CODE']
            ),
            false,
            array("UF_ELEMENT_NAME"),
            false
        );
        ?>

        <h2 class="align-center">
            <? if ($elementName = $rsElementName->GetNext()) : ?>

                <span class="dark-red">

                    <?= $elementName["UF_ELEMENT_NAME"]; ?>

                </span>

            <? endif; ?>

            модели

            <? echo $arResult['NAME']; ?>
        </h2>

        <? $props = $arResult['DISPLAY_PROPERTIES']; ?>

        <section id="short-properties" class="grid">

            <ul class="list">

                <li class="first item">
                    <span class="title dark-red"><?= $props['carry_weight']['NAME'] ?></span>
                    <span class="value tonn"><?= $props['carry_weight']['VALUE'] ?></span>
                </li>

                <li class="item">
                    <span class="title dark-red"><?= $props['tare_weight']['NAME'] ?></span>
                    <span class="value tonn"><?= $props['tare_weight']['VALUE'] ?></span>
                </li>

                <li class="item">
                    <span class="title dark-red"><?= $props['base_wight']['NAME'] ?></span>
                    <span class="value millimeter"><?= $props['base_wight']['VALUE'] ?></span>
                </li>

                <li class="last item">
                    <span class="title dark-red"><?= $props['car_volume']['NAME'] ?></span>
                    <span class="value cubic-meter"><?= $props['car_volume']['VALUE'] ?></span>
                </li>

            </ul>

        </section>
    </div>


    <div id="view-wrap">


        <section id="3dview" class="grid">

            <? foreach ($arResult['DISPLAY_PROPERTIES']['rotated_images']['FILE_VALUE'] as $i => $image) : ?>

                <?
                $thumb = CFile::ResizeImageGet(
                    $image,
                    array(
                        'width' => '800',
                        'height' => '600'
                    ),
                    BX_RESIZE_IMAGE_EXACT
                );

                $thumbs[] = $thumb['src'];

                ?>

            <? endforeach; ?>

            <? $thumbs = array_reverse($thumbs); ?>

            <div id='container'>
                <div id='spritespin' data-images="<? echo implode(",", $thumbs); ?>"></div>
                <div id='slider'></div>
            </div>

        </section>

        <div id="links" class="grid no-gutters">

            <aside id="" class="half unit">
                <a class="button call" href="/">Получить консультацию специалиста</a>
            </aside>

            <? if (count($arResult['SECTION_ELEMENTS']['ITEMS']) > 1) : ?>

                <nav id="section-elements" class="half unit align-right">

                    <span>Другие модели:</span>

                    <ul class="inline list">

                        <? $num = 0; ?>

                        <? foreach ($arResult['SECTION_ELEMENTS']['ITEMS'] as $element) { ?>

                            <? if ($element['ID'] !== $arResult['ID']): ?>

                                <? $items = count($arResult['ITEMS']) - 1; ?>

                                <li class="item <? if ($num == 0) : echo 'first';
                                elseif ($items == $num) : echo 'last'; endif; ?> ">

                                    <a href="<?= $element['DETAIL_PAGE_URL']; ?>">
                                        <?= $element['NAME']; ?>
                                    </a>

                                </li>

                                <? $num++; ?>

                            <? endif; ?>

                        <? } ?>
                    </ul>

                </nav>

            <? endif; ?>

        </div>

    </div>

    <section id="description" class="grid background-white">
        <div class="whole unit">

            <h2 class="title">Описание и <span class="dark-red">характеристики</span></h2>

            <p><?= $arResult["PREVIEW_TEXT"]; ?></p>

        </div>
    </section>

    <section id="properties" class="grid background-white">

        <div class="whole unit">

            <table>
                <tr>
                    <th>Параметры</th>
                    <? foreach ($arResult['SECTION_ELEMENTS']['ITEMS'] as $i => $arItem) : ?>
                        <? if ($arItem['ID'] == $arResult['ID']): ?>
                            <th><? echo $arItem['NAME']; ?></th>
                        <? endif; ?>
                    <? endforeach; ?>

                    <? foreach ($arResult['SECTION_ELEMENTS']['ITEMS'] as $i => $arItem) : ?>
                        <? if ($arItem['ID'] !== $arResult['ID']): ?>
                            <th><? echo $arItem['NAME']; ?></th>
                        <? endif; ?>
                    <? endforeach; ?>
                </tr>

                <? $num = 0; ?>

                <? foreach ($arResult['PROPERTIES'] as $i => $arProperty) : ?>

                    <? if ($arProperty['PROPERTY_TYPE'] !== 'F') : ?>

                        <? if ($arProperty['CODE'] == 'joint_axis_length' || $arProperty['CODE'] == 'width_top_in_light') : ?>

                            <tr class="<? if ($num % 2 === 0) : echo 'even';
                            else : echo 'odd'; endif; ?> property title">
                                <td colspan="<? echo count($arResult['SECTION_ELEMENTS']['ITEMS']) + 1; ?>">
                                    <? if ($arProperty['CODE'] == 'joint_axis_length') : ?>
                                        Длина
                                    <? elseif ($arProperty['CODE'] == 'width_top_in_light') : ?>
                                        Внутренние размеры кузова
                                    <? endif ?>
                                </td>
                            </tr>

                            <? $num++; ?>

                        <? endif ?>

                        <tr class="<? if ($num % 2 === 0) : echo 'even';
                        else : echo 'odd'; endif; ?> property <? echo strtolower($arProperty['CODE']); ?>">

                            <td class="name">
                                <? echo $arProperty['NAME']; ?>
                            </td>

                            <? foreach ($arResult['SECTION_ELEMENTS']['ITEMS'] as $i => $arItem) : ?>
                                <? if ($arItem['ID'] == $arResult['ID']): ?>
                                    <td class="value">
                                        <? echo $arItem['PROPERTY_' . strtoupper($arProperty['CODE']) . '_VALUE'] ?>
                                    </td>
                                <? endif; ?>
                            <? endforeach; ?>

                            <? foreach ($arResult['SECTION_ELEMENTS']['ITEMS'] as $i => $arItem) : ?>
                                <? if ($arItem['ID'] !== $arResult['ID']): ?>
                                    <td class="value">
                                        <? echo $arItem['PROPERTY_' . strtoupper($arProperty['CODE']) . '_VALUE'] ?>
                                    </td>
                                <? endif; ?>
                            <? endforeach; ?>

                        </tr>

                        <? $num = $num + 1; ?>

                    <? endif; ?>

                <? endforeach; ?>

            </table>

        </div>

    </section>

    <? if (!empty($arResult['DISPLAY_PROPERTIES']['gallery'])) : ?>

        <section id="gallery" class="grid background-white">

            <h2 class="title">Галерея</h2>

            <ul class="images inline list">

                <? foreach ($arResult['DISPLAY_PROPERTIES']['gallery']['FILE_VALUE'] as $picture) : ?>

                    <?

                    $thumb = CFile::ResizeImageGet(
                        $picture,
                        array(
                            'width' => '185',
                            'height' => '200'
                        ),
                        BX_RESIZE_IMAGE_EXACT
                    );

                    $preview = CFile::ResizeImageGet(
                        $picture,
                        array(
                            'width' => '600',
                            'height' => '480'
                        ),
                        BX_RESIZE_IMAGE_EXACT
                    );

                    ?>

                    <li class="item">
                        <a class="preview colorbox" href="<?= $preview['src']; ?>" rel="colorbox">
                            <img src="<?= $thumb['src'] ?>" alt="">
                        </a>
                    </li>

                <? endforeach; ?>

            </ul>

        </section>

    <? endif; ?>

    <aside class="whole unit background-white">
        <a class="button call" href="/">Получить консультацию специалиста</a>
    </aside>

</article>


<div class="hidden">
    <div id="consultation-form">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_RECURSIVE" => "Y",
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "hidden",
                "EDIT_TEMPLATE" => ""
            )
        ); ?>
    </div>
</div>