<?

$arProperties = ["ID", "IBLOCK_ID", "NAME", "DETAIL_PAGE_URL"];

foreach ($arParams['PROPERTY_CODE'] as $i => $property):

    $arDetailProperties[] = 'PROPERTY_' . strtoupper($property);

endforeach;

$arDisplayProperties = array_merge($arProperties, $arDetailProperties);

$rsElements = CIBlockElement::GetList(
    array(),
    array(
        "IBLOCK_CODE" => $arResult["IBLOCK_CODE"],
        "SECTION_CODE" => $arResult['SECTION']['CODE'],
        "ACTIVE" => "Y"
    ),
    false,
    false,
    $arDisplayProperties
);


while ($arElement = $rsElements->GetNext()) {

    $arResult['SECTION_ELEMENTS']['ITEMS'][$arElement['ID']] = $arElement;
    $arResult['SECTION_ELEMENTS']['PROPERTIES'] = $arDisplayProperties;

}

?>
