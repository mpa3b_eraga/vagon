$(document).on(
    'ready',
    function () {

        $('.slider', '#catalog-section').slick(
            {
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 1000,
                focusOnSelect: true,
                arrows: false,
                responsive:[
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            }
        );

    }
);