<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<? if (!empty($arResult['ITEMS'])) { ?>

    <? $rsSectionList = CIBlockSection::GetList(
        array("SORT"=>"ASC"),
        array("IBLOCK_CODE"=> "catalog"),
        false,
        array(),
        false
    ); ?>

    <h2>Наша продукция:</h2>

        <? while($arSection = $rsSectionList->GetNext()): ?>

           <h3>
               <a href="<?= $arSection["SECTION_PAGE_URL"]?>"><?= $arSection["NAME"];  ?></a>
           </h3>

        <? endwhile; ?>

    <h2>Модели вагонов:</h2>

    <? foreach($arResult['ITEMS'] as $item) : ?>

        <article class="car">

            <h3 class="title"><a href="<? echo $item['DETAIL_PAGE_URL']; ?>"><? echo $item['NAME']; ?></a></h3>

        </article>

    <? endforeach; ?>

<? } ?>
