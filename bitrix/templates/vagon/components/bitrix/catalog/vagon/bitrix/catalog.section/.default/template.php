<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>
<h2><?= $arResult['NAME']?></h2>

<? if (!empty($arResult['ITEMS'])) { ?>

    <article id="catalog-section">

        <div id="catalog-section-items-list" class="grid">
            <div class="whole unit">

                <span class="dark-red">Модели:</span>

                <ul class="inline list">

                    <? $items = count($arResult['ITEMS']) - 1; ?>

                    <? foreach ($arResult['ITEMS'] as $i => $arItem) { ?>
                        
                        <li class="item <? if($i == 0) : echo 'first';  elseif($items == $i) : echo 'last'; endif;?> ">
                            <a class="title" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><? echo $arItem['NAME']; ?></a>
                        </li>

                    <? } ?>

                </ul>

            </div>
        </div>

        <section id="catalog-section-description" class="grid">
            <div class="half unit">
                <?= $arResult["DESCRIPTION"] ?>
            </div>
        </section>

        <? $picture = CFile::ResizeImageGet(
            $arResult['PICTURE'],
            array(
                "width" => 895,
                "height" => 290
            ),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        ); ?>

        <div id="catalog-section-image" class="grid">
            <div class="whole unit align-center">
               <img class="picture" src="<?= $picture['src'] ?>" alt="<?= $arResult['NAME']?>">
            </div>
        </div>
        
        <section id="catalog-section-gallery" class="grid">

            <div class="gallery slider">

                <? foreach ($arResult['ITEMS'] as $key => $arItem) { ?>

                    <div class="item">


                            <? $picture = CFile::ResizeImageGet(
                                $arItem['PREVIEW_PICTURE'],
                                array(
                                    "width" => 110,
                                    "height" => 55
                                ),
                                BX_RESIZE_IMAGE_EXACT,
                                true
                            ); ?>

                            <img src="<?= $picture['src'] ?>" alt="">

                        <h3 class="title">
                            <a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><? echo $arItem['NAME']; ?></a>
                        </h3>


                    </div>
    
                <? } ?>
    
            </div>
        </section>

    </article>

    <div style="clear: both;"></div>

    <?

  }

?>
