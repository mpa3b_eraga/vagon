$(document).on(
    'ready',
    function () {

        var spin = $('#spritespin'),
            slide = $('#slider'),
            images = spin.data('images'),
            source = images.split(',');

        spin.spritespin(
            {
                source: source,
                frames: source.length,
                frame: 17,
                sense: 1,
                width: spin.width(),
                height: $('#container').height(),
                animate: false,
                onLoad: function () {

                    slide.slider(
                        {
                            min: 0,
                            max: source.length,
                            slide: function (e, ui) {

                                var api = spin.spritespin('api');

                                api.stopAnimation();
                                api.updateFrame(ui.value);

                            }
                        }
                    );

                },
                onFrame: function (event, data) {
                    slide.slider('value', data.frame);
                }
            }
        );

        $('a.call', 'aside').on(
            'click',
            function (e) {

                e.preventDefault();

                $.colorbox(
                    {
                        'inline': true,
                        'href': '#consultation-form',
                        'open': true,
                        'width': '400px',
                        'height': 'auto',
                        'scrolling': false
                    }
                )
            }
        );        

        $('input', '#question-page_url').val(window.location.href);

        function getURLParameter(name) {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
        }


        if (getURLParameter('formresult') == 'addok' || $('.errortext', '#consultation-form').length) {

            $.colorbox(
                {
                    'inline': true,
                    'href': '#consultation-form',
                    'open': true,
                    'width': '400px',
                    'height': 'auto',
                    'scrolling': false
                }
            )

        }


    }
);

