<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <nav id="section-menu">
        <ul class="menu">
            <?
            foreach ($arResult as $arItem):

                if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                    continue;
                ?>
                <? if ($arItem["SELECTED"]):?>
                    <li class="active item">
                        <a href="<?= $arItem["LINK"] ?>" data-caption="<?= $arItem["PARAMS"]['caption']?>" ><?= $arItem["TEXT"] ?></a>
                    </li>
                <? else:?>
                    <li class="item">
                        <a href="<?= $arItem["LINK"] ?>" data-caption="<?= $arItem["PARAMS"]['caption']?>"><?= $arItem["TEXT"] ?></a>
                    </li>
                <? endif ?>
            <? endforeach ?>
        </ul>
    </nav>
<? endif ?>
