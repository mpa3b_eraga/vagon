/**
 * Created by ilya_ on 05.10.2015.
 */

$(document).on(
    'ready',
    function () {

        $('li', '#main-menu').on(
            'mouseover',
            function () {
                $('ul', this).show();
            }
        );

        $('li','#main-menu').on(
            'mouseleave',
            function () {
                $('ul', this).hide();
            }
        );

        // Create default option "Go to..."
        //         $("<option />", {
        //             "selected": "selected",
        //             "value"   : "",
        //             "text"    : "Перейти"
        //         }).appendTo("#mobile-menu");

        $("#mobile-menu").change(function() {
            window.location = $('option:selected', this).val();
        });

    }
);

