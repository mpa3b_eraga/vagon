<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>

    <nav  id="main-menu">

        <ul class="menu desktop-only" >

        <? $previousLevel = 0;

        foreach ($arResult as $arItem): ?>

            <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
            <? endif ?>

            <? if ($arItem["IS_PARENT"]): ?>

                <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                    <li class="parent item<? if ($arItem["SELECTED"]) :?> active<? endif; ?>">
                        <a href="<?= $arItem["LINK"] ?>">
                            <?= $arItem["TEXT"] ?>
                        </a>
                        <ul>
                    <? else: ?>
                    <li class="parent item<? if ($arItem["SELECTED"]):?>active<? endif ?>">
                        <a href="<?= $arItem["LINK"] ?>">
                            <?= $arItem["TEXT"] ?>
                        </a>
                        <ul>
                <? endif ?>

            <? else: ?>

                <? if ($arItem["DEPTH_LEVEL"] == 1):?>
                    <li class="item <? if ($arItem["SELECTED"]):?>active<? endif ?>">
                        <a href="<?= $arItem["LINK"] ?>">
                            <?= $arItem["TEXT"] ?>
                        </a>
                    </li>
                <? else:?>
                    <li class="item">
                        <a href="<?= $arItem["LINK"] ?>">
                            <?= $arItem["TEXT"] ?>
                        </a>
                    </li>
                <? endif ?>

            <? endif ?>

            <? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>

        <? endforeach ?>

        <? if ($previousLevel > 1)://close last item tags?>
            <?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
        <? endif ?>

        </ul>


        <select id="mobile-menu" class="only-on-mobiles">

            <option value="/">Главная</option>

            <? foreach ($arResult as $arItem): ?>

                <? if ($arItem["DEPTH_LEVEL"] == 1):?>

                    <option value="<?= $arItem["LINK"] ?>"
                            <? if ($arItem["SELECTED"]):?>selected="selected"<? endif ?>>
                        <?= $arItem["TEXT"] ?>
                    </option>

                <? endif; ?>

            <? endforeach ?>

        </select>

    </nav>

<? endif ?>

