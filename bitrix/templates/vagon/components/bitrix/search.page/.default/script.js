$(document).on(
    'ready',
    function () {

        $('ul.results.list', '#search-result').niceScroll(
            {
                cursorcolor: '#858482',
                cursoropacitymin: 1,
                cursorborder: 'none',
                autohidemode: false,
                background: "#2C2C34"
            }
        );

        $('a.close-btn', '#search-result').on(
            'click',
            function (e) {

                e.preventDefault();
                
                $('input[name="q"]', '#search-result').val('');

            }
        )

    }
);
