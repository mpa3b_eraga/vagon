<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . '/style/' . $arParams['AREA_FILE_SUFFIX'] . '.css');
?>

<? if($arResult["FILE"] <> '') : ?>
	<aside <? if($arParams['AREA_FILE_SUFFIX']) : ?>id="<? echo $arParams['AREA_FILE_SUFFIX']; ?>" <? endif; ?> class="grid">
		<? if($arResult["FILE"] <> '') include($arResult["FILE"]); ?>
	</aside>
<? endif; ?>