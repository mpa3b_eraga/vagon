<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult)) return "";

$strReturn = '<nav id="breadcrumb" class="center-on-mobiles">';

$num_items = count($arResult);

for ($index = 0, $itemSize = $num_items; $index < $itemSize; $index++) {
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);

    if ($arResult[$index]["LINK"] <> "" && $index == 0)
        $strReturn .= '<a class="first"  href="' . $arResult[$index]["LINK"] . '" title="' . $title . '">' . $title . '</a> » ';

    elseif ($arResult[$index]["LINK"] <> "" && $index != $itemSize - 1)
        $strReturn .= '<a href="' . $arResult[$index]["LINK"] . '" title="' . $title . '">' . $title . '</a> » ';

    else $strReturn .= '<span class="last">' . $title . '</span>';

}

$strReturn .= '</nav>';

return $strReturn;
?>

