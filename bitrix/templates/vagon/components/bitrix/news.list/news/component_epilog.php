<?php

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/slick.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/ion.rangeSlider.js');

$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/slick.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/ion.rangeSlider.css');

?>