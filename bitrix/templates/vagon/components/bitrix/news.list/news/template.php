<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


global $USER;

?>

<div id="frontpage-news">

    <div id="frontpage-news-description">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
                "AREA_FILE_SHOW" => "page",
                "AREA_FILE_SUFFIX" => "news",
                "COMPONENT_TEMPLATE" => ".default",
                "EDIT_TEMPLATE" => "",
                "AREA_FILE_RECURSIVE" => "N"
            ),
            false
        ); ?>
    </div>

    <div id="frontpage-news-list" class="news-list grid">

        <? $years = array(); ?>

        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>

            <?
            $year = CIBlockFormatProperties::DateFormat("Y", strtotime($arItem["ACTIVE_FROM"]));
            $years[] = $year;
            ?>

            <article data-year="<? echo $year; ?>" <? if ($USER->isAdmin()) : ?>id="<?= $this->GetEditAreaId($arItem['ID']); ?>"<? endif; ?>
                     class="news item">

                <? if (is_array($arItem["PREVIEW_PICTURE"])): ?>

                    <? $img = CFile::ResizeImageGet(
                        $arItem['PREVIEW_PICTURE'],
                        array(
                            'width' => 230,
                            'height' => 230
                        ),
                        BX_RESIZE_IMAGE_EXACT
                    ); ?>

                    <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
                        <img class="image" src="<? echo $img['src']; ?>"/>
                    </a>

                <? endif; ?>

                <div class="content">
                    <h3><a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a></h3>
                    <span class="date dark-red"><? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span>

                    <? if(!empty($arItem["PREVIEW_TEXT"])) : ?>
                        <p><? echo $arItem["PREVIEW_TEXT"]; ?></p>
                    <? endif; ?>
                </div>

            </article>

        <? endforeach; ?>

    </div>

    <? $years = array_unique($years); ?>

    <div id="frontpage-news-more-link" class="grid align-right">
        <a href="/news/">Читать все новости</a>
    </div>

<? /*
    <div id="frontpage-news-years">
        <ul id="frontpage-news-years-list">
            <? foreach ($years as $year) : ?>
                <li class="year">
                    <a href="#year-<? echo $year; ?>" data-year="<? echo $year; ?>"><? echo $year; ?></a>
                </li>
            <? endforeach; ?>
        </ul>
        <div class="caret draggable"></div>
    </div>
*/ ?>
    
    <div class="range-slider">
        <input type="text" class="js-range-slider" value="" />
    </div>
    
</div>