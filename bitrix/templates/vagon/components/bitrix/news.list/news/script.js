$(document).on(
    'ready',
    function () {

        var items = $('.item', '#frontpage-news-list'),
            dates = [],
            dateMin = 0,
            dateMax = 0;

        items.each(
            function () {
                dates.push($(this).data('year'));
            }
        );

        dateMin = Math.min.apply(null, dates);
        dateMax = Math.max.apply(null, dates);

        $('#frontpage-news-list').slick(
            {
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                rows: 2,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            }
        );


        var $range = $(".js-range-slider");

         var reverse = function (num) {
             return dateMax + (dateMin-num);
         /*return min + (max-num);*/
         };

        var slickToIndex = function(year) {
            var item = $('.item[data-year=' + year + ']').closest('.slick-slide').not('.slick-cloned'),
                index = $(item, '#frontpage-news-list').data('slick-index');

            $('#frontpage-news-list').slick('goTo', index);

            $('.irs-grid-text:contains(' + year + ')').addClass('year-active');

        };

        $range.ionRangeSlider(
            {
                type: "single",
                min: dateMin,
                max: dateMax,
                grid: true,
                grid_snap: true,
                step: 1,
                hide_from_to: true,
                hide_min_max: true,
                prettify_separator: "",
                prettify: function (num) {
                    return reverse(num);
                },
                onStart: function (data) {

                    $('.irs-grid-text:contains(' + data.to + ')').addClass('year-active');

                },
                onChange: function () {

                    $('.irs-grid-text').removeClass('year-active');

                },
                onFinish: function (data) {

                    slickToIndex(reverse(data.from));
                }

            }
        );


        $('#frontpage-news-list').on(
            'swipe',
            function(event, slick, currentSlide, nextSlide){

                var currentSlideYear = $('.item', '.slick-current', this).data('year'),
                    slider = $range.data("ionRangeSlider");
                
                slider.update({
                    from: reverse(currentSlideYear)
                });

                $('.irs-grid-text:contains(' + currentSlideYear + ')').addClass('year-active');

            }
        );

        $( '#frontpage-news').on(
            'click',
            '.irs-grid-text',
            function () {

                slickToIndex(($(this).text()));

                $range.data("ionRangeSlider").update({
                    from: reverse($(this).text())
                });

                $('.irs-grid-text:contains(' + $(this).text() + ')').addClass('year-active');

            }
        );

    }
);