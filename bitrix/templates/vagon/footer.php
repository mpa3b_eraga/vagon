<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

			</main>

			<? $APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"aside",
				array(
					"AREA_FILE_SHOW" => "sect",
					"AREA_FILE_SUFFIX" => "bottom",
					"COMPONENT_TEMPLATE" => "aside",
					"EDIT_TEMPLATE" => "",
					"AREA_FILE_RECURSIVE" => "Y",
					"HTML_ID" => "bottom"
				),
				false
			);?>

			<footer id="footer" class="wrap">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "sect",
						"AREA_FILE_SUFFIX" => "footer",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => ""
					)
				);?>
			</footer>

		</div>

	</div>

	<noindex>
		<!-- JS Scripts -->
		<? $APPLICATION->ShowHeadScripts(); ?>
	</noindex>

	</body>
</html>