$(document).on(
    'ready',
    function () {

        $('a.colorbox').colorbox();

        $('a.colorbox-inline').colorbox(
            {
                inline: true
            }
        );


        // colorbox settings redefine


        if ($().colorbox()) {

            $.extend(
                $.colorbox.settings, {
                    opacity: 0.2,
                    close: 'закрыть',
                    current: 'Изображение {current} из {total}',
                    previous: 'предыдущий',
                    next: 'следующий',
                    xhrError: 'Ошибка загрузки',
                    imgError: 'Ошибка загрузки изображения',
                    scalePhotos: true
                }
            );

        }


        if ($('#page').height() < $(window).height()) {
            $('#page').height($(window).height());
        }
        

        $('li', '#section-menu').on(
            'click',
            function(e) {

                e.preventDefault();

                var id = $('a', this).data('caption');

                $('.section-info').hide();

                $('li', '#section-menu').removeClass('active');

                $(this).addClass('active');

                $(this).parents('#tech-section').find(id).show();
            }
        );
        
        $('a.close-btn', '#tech-section').on(
            'click',
            function(e) {

                e.preventDefault();

                $(this).parents('.section-info').hide();
                $('li', '#section-menu').removeClass('active');

            }
        )
               

    }
);
