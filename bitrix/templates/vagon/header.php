<!DOCTYPE html>

<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

// CSS
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/normalize.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/grid.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/common.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/layout.css');

$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/cera.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/lato.css');

$APPLICATION->SetAdditionalCSS($APPLICATION->GetCurDir() . 'style.css');

// JS
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/css-ua.js');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.js');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.colorbox.js');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/colorbox.css');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/common.js');

if ($APPLICATION->GetCurPage() === '/') {
	$path = 'front';
	$path_classes = 'page wrap';
	$page_id = $path . '-page';
}
else {
	$path = explode("/", trim($APPLICATION->GetCurPage(), '/'));
	$path_classes = implode(' ', $path) . ' page wrap';
	$page_id = implode('-', $path) . '-page';
};

global $USER,$APPLICATION;

if($USER->IsAdmin() == false) $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/prefixfree.js');

?>

	<head>

		<title><?$APPLICATION->ShowTitle();?></title>

		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

		<meta name="viewport"
			  content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">

		<meta charset="<? echo LANG_CHARSET; ?>">

		<link rel="shortcut icon" type="image/x-icon" href="<? echo SITE_TEMPLATE_PATH; ?>/favicon.ico"/>
		<link rel="icon" type="image/x-icon" href="<? echo SITE_TEMPLATE_PATH; ?>/favicon.ico"/>

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- META -->
		<? $APPLICATION->ShowMeta("robots"); ?>
		<? $APPLICATION->ShowMeta("keywords"); ?>
		<? $APPLICATION->ShowMeta("description"); ?>

		<!-- CSS -->
		<? $APPLICATION->ShowCSS(); ?>

		<!-- JS Strings -->
		<? $APPLICATION->ShowHeadStrings(); ?>

	</head>

	<body id="<? echo $page_id; ?>" class="<? echo $path_classes; ?>">

		<? if($USER->IsAdmin()) : ?>
			<noindex>
				<div id="panel">
					<? $APPLICATION->ShowPanel(); ?>
				</div>
			</noindex>
		<? endif; ?>

		<div id="page" class="grid">
			<header id="header" class="one-quarter unit">
				<div class="wrapper">

					<div id="site-description" class="align-center">
						<? if ($APPLICATION->GetCurPage() === '/') : ?>
							<img src="<? echo SITE_TEMPLATE_PATH . '/logo.png'; ?>" alt="Промтрактор - вагон">
						<? ; else : ?>
							<a href="/">
								<img src="<? echo SITE_TEMPLATE_PATH . '/logo.png'; ?>" alt="Промтрактор - вагон">
							</a>
						<? endif; ?>

						<p id="site-name" class="uppercase cera">Промтрактор - вагон</p>
					</div>

					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						".default",
						array(
							"AREA_FILE_SHOW" => "sect",
							"AREA_FILE_SUFFIX" => "kku",
							"COMPONENT_TEMPLATE" => ".default",
							"EDIT_TEMPLATE" => "",
							"AREA_FILE_RECURSIVE" => "Y"
						),
						false
					);?>

					<?$APPLICATION->IncludeComponent(
					"bitrix:menu",
					"main",
					array(
						"ALLOW_MULTI_SELECT" => "N",
						"CHILD_MENU_TYPE" => "left",
						"COMPONENT_TEMPLATE" => "main",
						"DELAY" => "N",
						"MAX_LEVEL" => "3",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"ROOT_MENU_TYPE" => "main",
						"USE_EXT" => "Y"
					),
					false
				);?>

					<?$APPLICATION->IncludeComponent(
	"bitrix:search.form", 
	"main", 
	array(
		"COMPONENT_TEMPLATE" => "main",
		"PAGE" => "/search/",
		"USE_SUGGEST" => "Y"
	),
	false
);?>

					<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "left",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"CALC_URL" => "http://барбер.рф/rasschitat-effekt.html"
	),
	false
);?>

				</div>
			</header>

			<div id="main" class="three-quarters unit">
				<main id="content" class="grid">

					<div class="header grid no-gutters">

						<? if($APPLICATION->GetdirProperty('HIDE_TITLE') !== 'Y') : ?>
							<h1 class="center-on-mobiles"><? echo $APPLICATION->ShowTitle(false); ?></h1>
						<? endif; ?>

						<? if ($APPLICATION->GetCurPage() !== '/') : ?>

							<?$APPLICATION->IncludeComponent(
								"bitrix:breadcrumb",
								"breadcrumb",
								array(
									"COMPONENT_TEMPLATE" => "breadcrumb",
									"PATH" => "",
									"SITE_ID" => "-",
									"START_FROM" => "0"
								),
								false
							);?>

						<? endif; ?>

					</div>
